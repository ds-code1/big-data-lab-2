from pymongo import MongoClient
from typing import Final
from pprint import pprint
import json

CONNECTION_STRING: Final[str] = "mongodb://localhost:27017"


def get_database(database_name: str, connection_string: str):
    client = MongoClient(connection_string)
    return client[database_name]


if __name__ == "__main__":
    # Connect to MongoDB database
    db = get_database("testdb", CONNECTION_STRING)

    # Create empty collection
    collection = db["test_collection"]

    # Create document from JSON format
    with open('test_document.json', 'r') as json_file:
        doc_from_json = json.load(json_file)

    # Create 4 different documents
    doc1 = {
        "title": "Working",
        "author": "somedude",
        "datalist": [
            "one",
            "second",
            "last"
        ],
        "url": "https://instagram.com/"
    }

    doc2 = {
        "widget": {
            "debug": "on",
            "window": {
                "title": "Sample Konfabulator Widget",
                "name": "main_window",
                "width": 500,
                "height": 500
            }
        }
    }

    doc3 = {
        "menu": {
            "header": "SVG Viewer",
            "items": [
                {"id": "Open"},
                {"id": "OpenNew", "label": "Open New"},
                None
            ]
        }
    }

    doc4 = {
        "glossary": {
            "title": "example glossary",
            "GlossDiv": {
                "title": "S", }
        }
    }
