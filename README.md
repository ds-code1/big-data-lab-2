Test work

1. Create a connection to MongoDB
2. Create a collection of documents
3. Write a sample document in JSON format
4. Add four different documents to the collection
5. Output a list of the documents in the collection with all attributes by running a
database query
6. Output the main attributes of the part of the documents in the collection that satisfy some condition (use "less than", "more than" conditions)